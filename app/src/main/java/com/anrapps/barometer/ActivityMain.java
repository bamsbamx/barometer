package com.anrapps.barometer;

import android.annotation.TargetApi;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.widget.TextView;

import com.anrapps.barometer.util.BarometerManager;
import com.anrapps.barometer.util.UIUtil;

public class ActivityMain extends AppCompatActivity implements SensorEventListener {

    private BarometerManager mBarometerManager;

    private CoordinatorLayout mCoordinatorLayout;
    private Toolbar mToolbar;
    private FloatingActionButton mFab;
    private View mContentView;

    private TextView mTextView;

    private int mStatusBarHeight, mNavigationBarHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mContentView = findViewById(R.id.content_scroll_view);

        setSupportActionBar(mToolbar);

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                final Snackbar snackbar = Snackbar.make(mCoordinatorLayout, "Replace with your own action", Snackbar.LENGTH_LONG);
                UIUtil.displaySnackBarWithBottomMargin(snackbar, mNavigationBarHeight);
            }
        });

        setUpInsetViewsIfPossible();

        mBarometerManager = new BarometerManager(this);
        if (!mBarometerManager.isBarometerAvailable()) {
            final Snackbar snackbar = Snackbar.make(mCoordinatorLayout, R.string.error_barometer_not_available, Snackbar.LENGTH_LONG)
                    .setAction(R.string.exit, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
            UIUtil.displaySnackBarWithBottomMargin(snackbar, mNavigationBarHeight);
            return;
        } else {
            if (!BarometerService.isServiceAlarmScheduled(this))
                BarometerService.scheduleServiceAlarm(this);
        }

        mTextView = (TextView) findViewById(R.id.text);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBarometerManager.registerListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBarometerManager.unregisterListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        mTextView.setText("Pressure: " + event.values[0] + " mBar\n");
        mTextView.append("Accuracy: " + event.accuracy);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    @TargetApi(Build.VERSION_CODES.KITKAT_WATCH)
    private void setUpInsetViewsIfPossible() {
        if (!UIUtil.minSdkVersion21()) return;
        mCoordinatorLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mCoordinatorLayout.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
            @Override public WindowInsets onApplyWindowInsets(View v, WindowInsets insets) {

                mStatusBarHeight = insets.getSystemWindowInsetTop();
                mNavigationBarHeight = insets.getSystemWindowInsetBottom();

                final ViewGroup.MarginLayoutParams coordinatorLp = (ViewGroup.MarginLayoutParams) mCoordinatorLayout.getLayoutParams();
                coordinatorLp.topMargin += mStatusBarHeight;
                mCoordinatorLayout.setLayoutParams(coordinatorLp);

                final ViewGroup.MarginLayoutParams toolbarLp = (ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams();
                toolbarLp.rightMargin += insets.getSystemWindowInsetRight();
                mToolbar.setLayoutParams(toolbarLp);


                final ViewGroup.MarginLayoutParams fabLp = (ViewGroup.MarginLayoutParams) mFab.getLayoutParams();
                fabLp.bottomMargin += mNavigationBarHeight;
                fabLp.rightMargin += insets.getSystemWindowInsetRight();
                mFab.setLayoutParams(fabLp);

                mContentView.setPadding(mContentView.getPaddingLeft(),
                        mContentView.getPaddingTop(),
                        mContentView.getPaddingRight() + insets.getSystemWindowInsetRight(),
                        mContentView.getPaddingBottom() + mNavigationBarHeight);

                //do not re-apply window insets
                mCoordinatorLayout.setOnApplyWindowInsetsListener(null);

                return insets.consumeSystemWindowInsets();
            }
        });
    }

}
