package com.anrapps.barometer.util;

import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;

public class UIUtil {

    public static boolean minSdkVersion21() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static void displaySnackBarWithBottomMargin(Snackbar snackbar, int marginBottom) {

        final View snackBarView = snackbar.getView();
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) snackBarView.getLayoutParams();

        params.setMargins(
                params.leftMargin,
                params.topMargin,
                params.rightMargin,
                params.bottomMargin + marginBottom);

        snackBarView.setLayoutParams(params);

        snackbar.show();
    }

}
