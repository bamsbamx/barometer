package com.anrapps.barometer.util;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class BarometerManager {

    private SensorManager mSensorManager;
    private Sensor mPressureSensor;

    public BarometerManager(Context context) {
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mPressureSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
    }

    public boolean isBarometerAvailable() {
        return mPressureSensor != null;
    }

    public void registerListener(SensorEventListener listener) {
        if (isBarometerAvailable())
            mSensorManager.registerListener(listener, mPressureSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void unregisterListener(SensorEventListener listener) {
        if (isBarometerAvailable())
            mSensorManager.unregisterListener(listener);
    }



}
