package com.anrapps.barometer.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.anrapps.barometer.provider.BarometerContract.StatColumns;

public class DataProvider extends ContentProvider {
   
    public static final int URI_MATCH_STATS = 1;
    public static final int URI_MATCH_STATS_ID = 2;
	
	private static final UriMatcher sUriMatcher = buildUriMatcher();
	static UriMatcher buildUriMatcher() {
        final String authority = BarometerContract.CONTENT_AUTHORITY;
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(authority, BarometerContract.PATH_STATS, URI_MATCH_STATS);
        matcher.addURI(authority, BarometerContract.PATH_STATS + "/*", URI_MATCH_STATS_ID);
        return matcher;
    }
	
	private DatabaseHelper mDatabaseHelper;

    @Override
    public boolean onCreate() {
        mDatabaseHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case URI_MATCH_STATS:
                return StatColumns.CONTENT_TYPE;
            case URI_MATCH_STATS_ID:
                return StatColumns.CONTENT_ITEM_TYPE;
             default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {
            case URI_MATCH_STATS:
                retCursor = mDatabaseHelper.getReadableDatabase().query(
					StatColumns.TABLE_NAME,
					projection,
					selection,
					selectionArgs,
					null,
					null,
					sortOrder
                );
                break;
            case URI_MATCH_STATS_ID:
                String id = uri.getLastPathSegment();
                retCursor = mDatabaseHelper.getReadableDatabase().query(
					StatColumns.TABLE_NAME,
					projection,
					StatColumns._ID + " = ?",
					new String[]{id},
					null,
					null,
					sortOrder
                );
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (getContext() != null)
            retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        Uri returnUri;
        switch (sUriMatcher.match(uri)) {
            case URI_MATCH_STATS:
                long _id = db.insert(StatColumns.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = StatColumns.buildStatUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        notifyContentResolverChange(getContext(), uri);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        int rowsDeleted;
        // this makes delete all rows return the number of rows deleted
        if (TextUtils.isEmpty(selection)) selection = "1";
        switch (sUriMatcher.match(uri)) {
            case URI_MATCH_STATS:
                rowsDeleted = db.delete(
					StatColumns.TABLE_NAME,
					selection,
					selectionArgs
                );
                break;
            case URI_MATCH_STATS_ID:
                String _id = uri.getLastPathSegment();
                rowsDeleted = db.delete(
					StatColumns.TABLE_NAME,
					StatColumns._ID + " = ?",
					new String[]{_id}
                );
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsDeleted != 0) notifyContentResolverChange(getContext(), uri);
        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        int rowsUpdated;
        switch (sUriMatcher.match(uri)) {
            case URI_MATCH_STATS:
                rowsUpdated = db.update(
                        StatColumns.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs
                );
                break;
            case URI_MATCH_STATS_ID:
                String _id = uri.getLastPathSegment();
                rowsUpdated = db.update(StatColumns.TABLE_NAME,
                        values,
                        StatColumns._ID + " = ?",
                        new String[]{_id}
                );
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) notifyContentResolverChange(getContext(), uri);
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        final String tableName;
        switch (sUriMatcher.match(uri)) {
            case URI_MATCH_STATS:
                tableName = StatColumns.TABLE_NAME;
                break;
            case URI_MATCH_STATS_ID:
                throw new UnsupportedOperationException("Non sense to bulk insert for uri: " + uri);
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        db.beginTransaction();
        int returnCount = 0;
        try {
            for (ContentValues value : values) {
                long _id = db.insert(tableName, null, value);
                if (_id != -1) returnCount++;
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        notifyContentResolverChange(getContext(), uri);
        return returnCount;
    }

    private static void notifyContentResolverChange(Context context, Uri uri) {
        if (context == null) return;
        ContentResolver contentResolver = context.getContentResolver();
        contentResolver.notifyChange(uri, null, false);
    }

}
