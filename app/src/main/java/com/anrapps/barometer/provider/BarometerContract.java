package com.anrapps.barometer.provider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class BarometerContract {
	
    private BarometerContract() {}

	public static final String CONTENT_AUTHORITY = "com.anrapps.barometer.provider";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_STATS = "stats";

    public static class StatColumns implements BaseColumns {
       
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.barometer.stats";
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.barometer.stat";

   		public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_STATS).build();

        public static final String TABLE_NAME = PATH_STATS;
        
        public static final String COLUMN_NAME_PRESSURE = "pressure";
        public static final String COLUMN_NAME_ACCURACY = "accuracy";
        public static final String COLUMN_NAME_VALUE_COUNT = "value_count";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
		
		public static Uri buildStatUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
