package com.anrapps.barometer.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.anrapps.barometer.provider.BarometerContract.StatColumns;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "barometer.db";
	private static final int DATABASE_VERSION = 2;

	private static final String TYPE_INT = " INTEGER";
	private static final String TYPE_REAL = " REAL";
    private static final String TYPE_TEXT = " TEXT";
    private static final String NOT_NULL = " NOT NULL";
    private static final String COMMA_SEP = ", ";

    public DatabaseHelper(Context c) {
		super(c, DATABASE_NAME, null, DATABASE_VERSION);
    }

	@Override
	public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + StatColumns.TABLE_NAME + " (" +
				   StatColumns._ID + TYPE_INT + " PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
				   StatColumns.COLUMN_NAME_PRESSURE + TYPE_REAL + NOT_NULL + COMMA_SEP +
				   StatColumns.COLUMN_NAME_ACCURACY + TYPE_REAL + COMMA_SEP +
				   StatColumns.COLUMN_NAME_VALUE_COUNT + TYPE_INT + COMMA_SEP +
				   StatColumns.COLUMN_NAME_TIMESTAMP + TYPE_TEXT + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL("DROP TABLE IF EXISTS " + StatColumns.TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}
