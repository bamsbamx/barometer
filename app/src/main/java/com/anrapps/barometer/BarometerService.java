package com.anrapps.barometer;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.net.Uri;
import android.util.Log;

import com.anrapps.barometer.provider.BarometerContract.StatColumns;
import com.anrapps.barometer.util.BarometerManager;

public class BarometerService extends IntentService implements SensorEventListener {

    private static final String WORKER_NAME = "BarometerService";
    private static final int MIN_VALUE_COUNT = 5;

    private int mValueCount = 0;
    private float[] mPressureValues = new float[100];
    private int mAccuracy = 0;

    public BarometerService() {
        super(WORKER_NAME);
    }


    /**
     *  Since this service gets killed when onHandleIntent method finishes, the service must wait until
     *  it receives at least one value from the sensor. Here it will get at least MIN_VALUE_COUNT to
     *  get the average pressure
     * @param intent Default intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {

        final BarometerManager barometerManager = new BarometerManager(this);

        if (!barometerManager.isBarometerAvailable()) {
            Log.e(getClass().getName(), "No pressure sensor available");
            return;
        }

        barometerManager.registerListener(this);

        //Wait until some values are received
        while (mValueCount < MIN_VALUE_COUNT) {
            try { Thread.sleep(500); }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        barometerManager.unregisterListener(this);

        float valueSum = 0;
        for (float f : mPressureValues) valueSum += f;
        float averageValue = valueSum / mValueCount;


        final Uri insertUri = StatColumns.CONTENT_URI;
        final ContentValues cv = new ContentValues(3);
        cv.put(StatColumns.COLUMN_NAME_PRESSURE, averageValue);
        cv.put(StatColumns.COLUMN_NAME_ACCURACY, mAccuracy);
        cv.put(StatColumns.COLUMN_NAME_TIMESTAMP, System.currentTimeMillis());
        cv.put(StatColumns.COLUMN_NAME_VALUE_COUNT, mValueCount);
        getContentResolver().insert(insertUri, cv);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float f = event.values[0];
        mPressureValues[mValueCount] = f;
        mAccuracy = event.accuracy;
        mValueCount++;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    public static void scheduleServiceAlarm(Context context) {
        final AlarmManager alarmMgr = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        final Intent intent = new Intent(context, BarometerService.class);
        final PendingIntent alarmIntent = PendingIntent.getService(context, 0, intent, 0);

        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                //AlarmManager.INTERVAL_HALF_DAY,
                5000, //TODO: Just for testing. Set proper interval
                60000,
                alarmIntent
        );
    }

    public static boolean isServiceAlarmScheduled(Context context) {
        final Intent intent = new Intent(context, BarometerService.class);
        return PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_NO_CREATE) != null;
    }
}
